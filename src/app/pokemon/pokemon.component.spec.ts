import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from "@angular/router/testing";
import { ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Location } from '@angular/common';

import { PokemonComponent } from './pokemon.component';
import { PokemonService } from '../pokemon.service';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('PokemonComponent', () => {
  let component: PokemonComponent;
  let fixture: ComponentFixture<PokemonComponent>;
  let myService: PokemonService;
  let apolloTest: ApolloTestingModule;

  let route: ActivatedRoute;
  let locate: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PokemonComponent],
      imports: [
        ApolloTestingModule,
        RouterTestingModule,
        HttpClientModule
      ],
      // providers: [myService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
