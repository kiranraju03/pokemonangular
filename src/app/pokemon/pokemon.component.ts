import { Component, OnInit } from '@angular/core';
import { QueryRef, Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PokemonListItem } from '../pokemon';

import gpl from 'graphql-tag';

import { Observable } from 'rxjs';
import { PokemonService } from '../pokemon.service';


@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})

export class PokemonComponent implements OnInit {
  pokemon: Observable<PokemonListItem>;
  constructor(
    private apollo: Apollo,
    private pokemonService: PokemonService,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
    console.log("Pokemon Constructor");
  }

  pokemonQuery = gpl`
  query pokemons($first: Int!){
    pokemons(first: $first){
      id
      name
      image
    }
  }
  `;

  pokemons: PokemonListItem[] = [];

  private query: QueryRef<any>;
  loading: boolean;
  poke_count: number;

  getPokemonCountSelected(): void {
    this.poke_count = +this.activatedRoute.snapshot.paramMap.get('num');
    // this.pokemonService.getPokemonDetails(poke_num).subscribe(poke => this.pokemon = poke.pokemon);
  }

  goBack(): void {
    this.location.back();
  }

  ngOnInit(): void {
    this.getPokemonCountSelected();
    this.query = this.apollo.watchQuery({
      query: this.pokemonQuery,
      variables: { first: this.poke_count }
    });

    this.query.valueChanges.subscribe(result => {
      this.pokemons = result.data.pokemons;
      console.log(this.pokemons);
    });
    // console.log("ngoniti");
    // this.pokemonService.getPokemons().subscribe(poke_list => this.pokemons = poke_list.pokemonListItems);
    // console.log(this.pokemons);
  }

}
