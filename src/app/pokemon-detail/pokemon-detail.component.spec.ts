import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { PokemonDetailComponent } from './pokemon-detail.component';
import { PokemonService } from '../pokemon.service';
import { Location } from '@angular/common';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('PokemonDetailComponent', () => {
  let component: PokemonDetailComponent;
  let fixture: ComponentFixture<PokemonDetailComponent>;
  let teste: PokemonDetailComponent;
  let route: ActivatedRoute;
  let myService: PokemonService;
  let locate: Location;

  beforeEach(async(() => {
    teste = new PokemonDetailComponent(route, myService, locate);
    TestBed.configureTestingModule({
      declarations: [PokemonDetailComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        ApolloTestingModule
      ],
      // providers: [myService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
