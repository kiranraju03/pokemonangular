import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from '../pokemon';
import { Location } from '@angular/common';

import { PokemonService } from '../pokemon.service';
@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private pokemonService: PokemonService,
    private location: Location
  ) {
    console.log("Pokemon Details Constructor");
  }

  pokemon: Pokemon;

  ngOnInit(): void {
    this.getPokemonDetails();
  }

  goBackToList(): void {
    this.location.back();
  }


  getPokemonDetails(): void {
    const name = this.activatedRoute.snapshot.paramMap.get('name');
    this.pokemonService.getPokemonDetails(name).subscribe(poke => this.pokemon = poke.pokemon);
  }

}
