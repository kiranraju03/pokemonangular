import { Component, OnInit, Input } from '@angular/core';

import { PokemonListItem } from '../pokemon';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {

  @Input()
  pokemon: PokemonListItem;
  constructor() { }

  ngOnInit(): void {
  }

}
