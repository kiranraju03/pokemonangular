import { TestBed } from '@angular/core/testing';

import { PokemonService } from './pokemon.service';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('PokemonService', () => {
  let service: PokemonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ApolloTestingModule
      ],
    });
    service = TestBed.inject(PokemonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
