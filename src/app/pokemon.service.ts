import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Apollo, QueryRef } from 'apollo-angular';
import gpl from 'graphql-tag';
import { PokemonListItems, PokemonListItem, PokemonDetails } from './pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private apollo: Apollo) {
    console.log("Pokemon service constructor");
  }

  // private query: QueryRef<any>;

  pokemonQuery = gpl`
    query pokemons($first: Int!){
      pokemons(first: $first){
        id
        name
        image
      }
    }
  `;

  pokemonDetailsQuery = gpl`
  query pokemon($name: String){
    pokemon(name: $name){
      id
      number
      name
      classification
      fleeRate
      maxCP
      maxHP
      image
    }
  }
  `;

  getPokemonDetails(poke_name: string): Observable<PokemonDetails> {
    return this.apollo
      .query<PokemonDetails>({ query: this.pokemonDetailsQuery, variables: { name: poke_name } })
      .pipe(map(result => result.data));
  }

  // pokemons: Observable<PokemonListItem[]>;

  getPokemons(): Observable<PokemonListItems> {
    return this.apollo
      .query<PokemonListItems>({ query: this.pokemonQuery, variables: { first: 10 } })
      .pipe(map(result => result.data));
  }
}
