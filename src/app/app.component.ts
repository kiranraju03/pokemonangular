import { Component } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface PokemonSearchListItem {
  image: string;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Find your Pokemon!';
  pokemon_name: string;
  selectedValue: number;
  pokemonCount: number[] = [1, 5, 10, 15, 20, 25, 30];

}
