export interface Pokemon {
    id: string;
    number: string;
    name: string;
    classification: string;
    fleeRate: number;
    maxCP: number;
    maxHP: number;
    image: string;
}

export interface PokemonDetails {
    pokemon: Pokemon;
}

export interface PokemonListItem {
    id: string;
    name: string;
    image: string;
}

export interface PokemonListItems {
    pokemonListItems: PokemonListItem[];
}

// export interface PokemonList {
//     pokemonList: PokemonListItems;
// }